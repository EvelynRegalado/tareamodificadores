package com.otro;
import com.company.Padre;

/**
 * Evelyn Regalado
 * Modificador protected
 */
public class Niño extends Padre {
    public void prueba(){
        System.out.println("x es: "+ x);
        //En este caso se puede acceder a la variable
        // x protected por herencia
    }
}
