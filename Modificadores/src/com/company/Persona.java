package com.company;

/**
 * Created by Evelyn Regalado
 */
public class Persona {

    //***Definir constantes***

    //Género por defecto
    private final static char GeneroDefecto = 'H';
    //Debajo del peso
    public static final int DebPeso = -1;
    //Peso Ideal
    public static final int IdealPeso = 0;
    // Sobre Peso
    public static final int SobrePeso = 1;

    //***Definir los atributos***

    //Nombre
    private String nombre;
    //Edad
    private int edad;
    //Cédula - Se generará con un objeto
    //private String CI;
    //Género
    private char genero;
    //Peso
    private double peso;
    //Altura
    private double altura;

    //Constructores


    public Persona(String nombre, int edad, char genero, double peso, double altura) {
        this.nombre = nombre;
        this.edad = edad;
        this.genero = genero;
        this.peso = peso;
        this.altura = altura;
    }

    //Métodos private

    private void comprobarSexo() {

        //Si el sexo no es una H o una M, por defecto es H
        if (genero != 'H' && genero != 'M') {
            this.genero = GeneroDefecto;
        }
    }

    //Métodos publicos -- set


    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public void setGenero(char genero) {
        this.genero = genero;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    // Cálcuso de su indice de masa corporal

    public int CalculoIMC(){
        //Calculamos el peso de la persona
        double pesoActual;
        pesoActual = peso / (Math.pow(altura, 2));
        //Segun el peso, devuelve un codigo
        if (pesoActual >= 20 && pesoActual <= 25) {
            return IdealPeso;
        } else if (pesoActual < 20) {
            return DebPeso;
        } else {
            return SobrePeso;
        }
    }

    // Calculo de si la persona es mayor o menor de edad

    public boolean MayorMenorEdad(){
        boolean mayor;
        mayor = false;
        if (edad >= 18) {
            mayor = true;
        }
        return mayor;
    }

    //toString

    @Override
    public String toString() {
        String sexo;
        if (this.genero == 'H') {
            sexo = "hombre";
        } else {
            sexo = "mujer";
        }
        return "Informacion de la Persona: " +
                "nombre='" + nombre + '\'' +
                ", edad=" + edad +
                ", genero=" + genero +
                ", peso=" + peso +
                ", altura=" + altura +
                '}';
    }
}