package com.company;

/**
 * Created by Evelyn Regalado
 */
public class Circulo extends Figura {
    final double pi= 3.1416;
    private double radio;


    @Override
    public double CalcularArea() {

        return this.area=pi*radio*radio;
    }

    public double getRadio() {
        return radio;
    }

    public void setRadio(double radio) {
        this.radio = radio;
    }
}
