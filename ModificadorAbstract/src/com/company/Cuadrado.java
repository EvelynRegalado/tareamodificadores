package com.company;

import com.company.Figura;

/**
 * Created by Evelyn Regalado
 */
public class Cuadrado extends Figura{

    private double lado;

    @Override
    public double CalcularArea() {
        return this.area= lado*lado;
    }

    public double getLado() {
        return lado;
    }

    public void setLado(double lado) {
        this.lado = lado;
    }
}
