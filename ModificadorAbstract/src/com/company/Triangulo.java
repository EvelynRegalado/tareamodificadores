package com.company;

/**
 * Created by Evelyn Regalado
 */
    public class Triangulo extends  Figura{

    private double base;
    private double altura;

    @Override
    public double CalcularArea() {
        return this.area= base*altura/2;
    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }
}
