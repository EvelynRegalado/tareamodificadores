package com.company;

/**
 * Created by Evelyn Regalado
 */
public abstract class Figura {
    public double area;

    abstract public double CalcularArea();

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }
}
