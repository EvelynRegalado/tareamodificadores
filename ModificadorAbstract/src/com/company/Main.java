
package com.company;

public class Main {

    public static void main(String[] args) {

        Circulo circulo1 = new Circulo();
        Cuadrado cuadrado1 = new Cuadrado();
        Triangulo triangulo1 = new Triangulo();

        circulo1.setRadio(2);
        System.out.println("Area de Circulo "+ circulo1.CalcularArea());

        cuadrado1.setLado(2);
        System.out.println("Area de Cuadrado "+ cuadrado1.CalcularArea());

        triangulo1.setAltura(2);
        triangulo1.setBase(2);
        System.out.println("Area de Triangulo "+ triangulo1.CalcularArea());

    }
}
